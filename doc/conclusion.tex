\section{Conclusion, perspectives}

Pour conclure, notre programme offre les fonctionnalités suivantes :

\begin{itemize}

\item segmentation du visage et des cheveux avec suivi par la technique de \textit{Graph Cuts}
\item possibilité d'enregistrer les segmentations faites pour tester en modifiant les paramètres
\item possibilité d'utiliser la bibliothèque \texttt{FaceTracker} dans un autre exécutable et de récupérer les points d'intérêt du visage à la première frame
\newline
\end{itemize}

L'application offre, en sortie, une segmentation assez correcte tant sur la détection globale des régions du visage que sur la continuité temporelle. En revanche, sur des cas plus complexes comme lors d'un changement d'illumination ou une occlusion importante, la segmentation penne à détecter correctement les régions. Notre méthode de Graph-Cuts se base sur des informations de couleur, mais une utilisation des patchs de textures, par exemple, pourrait améliorer la stabilité de l'application. D'autres pistes exploitant une méthode de calcul du flot optique seraient un plus pour la qualité des segmentations.

Une autre perspective serait d'ajouter comme fonctionnalité au programme la possibilité de segmenter plusieurs visages avec des étiquettes différentes.

Enfin, l'intégration de l'interface de \texttt{FaceTracker} permettrait une navigation plus rapide et améliorerait l'ergonomie de l'application. Malgré ce point, l'utilisation de cette bibliothèque est parfaitement fonctionnelle et permet d'obtenir de bons résultats sur le visage.
