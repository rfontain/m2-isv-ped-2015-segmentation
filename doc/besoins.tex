\section{Analyse des besoins}
\label{sec:besoins}

Nous allons procéder à l'analyse des besoins fonctionnels et non fonctionnels de notre projet. Nous allons décrire les différents services que devra rendre notre logiciel en établissant leurs priorités.

\subsection{Besoins fonctionnels}

L’objectif du projet est de segmenter et suivre la segmentation des visages et cheveux dans une vidéo.

Le logiciel prend en entrée \textit{N} frames extraites d'un fichier vidéo et doit effectuer une segmentation des visages et des cheveux avec suivi de cette segmentation sur chaque frame successive. Le logiciel donne en sortie \textit{N} images contenant les masques de segmentation correspondant aux visages (blanc), aux cheveux (gris) et au fond (noir) obtenus pour chaque frame. 

La figure \ref{masque} ci-dessous montre un exemple d'image de sortie obtenue après segmentation de visage et de cheveux sur une frame contenant une personne. L'image de sortie est une image étiquettée constituée de trois masques correspondant aux trois régions du visage, des cheveux et du fond.

\begin{figure}[!h]
\begin{bigcenter}
\includegraphics[width=70mm]{img/exemple-masque.jpg}
\caption{Exemple de masques obtenus après segmentation sur une image : visage (blanc), cheveux (gris), fond (noir)}
\label{masque}
\end{bigcenter}
\end{figure}

Notre logiciel doit répondre aux besoins fonctionnels énoncés ci-dessous.

\subsubsection{Cas d'un ou plusieurs visages}

L'objectif prioritaire est de pouvoir effectuer une segmentation du visage et des cheveux sur une vidéo contenant une seule personne filmée. On effectue un partitionnement des frames en trois régions étiquettées correspondant au visage, aux cheveux et au fond (cf. Figure \ref{masque}).

L'objectif secondaire est de permettre d'effectuer une segmentation de visages et de cheveux sur une vidéo contenant plusieurs personnes filmées. Pour une vidéo contenant \textit{n} personnes, le programme permet d'étiquetter chaque frame avec \textit{n*2+1} labels: 1 label pour le fond et \textit{n} paires de labels correspondant aux \textit{n} paires de visages/cheveux.

\subsubsection{Robustesse de la segmentation}

Le logiciel doit pouvoir effectuer une segmentation sur une vidéo où la caméra reste fixe et où le sujet filmé peut se déplacer.

Notre logiciel doit fonctionner sur diff\'erentes orientations et expressions des visages des sujets :
\begin{itemize}
\item De face.
\item De trois quarts.
\item Visage fixe et diverses expressions (sourire, changement regard, en train de parler etc.).
\end{itemize}
Concernant la pilosit\'e faciale, nous prenons en compte qu'elle fait partie du visage, car elle peut entourer la bouche, et l'étiquetter comme les cheveux serait plus difficile, mais ce serait l'id\'eal.

La segmentation doit rester robuste en cas d'occlusions du visage (exemple: main qui passe devant le visage, visages croisés, etc.).

La segmentation doit aussi fonctionner en cas de changement d'illumination brusque dans la scène. 

\subsubsection{Continuité temporelle}

La segmentation doit s’aligner le plus possible sur les contours mais sans créer de saut temporel entre images successives.
Les masques de segmentation doivent rester continus au cours de la vidéo et ne pas être distordus (agrandissement soudain, déplacement intempestif...).

\subsubsection{Implémentation de plusieurs méthodes de segmentation}

Le projet consiste à implémenter et tester plusieurs méthodes de segmentation dans l'ordre de priorité suivant:
\begin{itemize}
\item une méthode basée sur les \textit{Graph-Cuts} \cite{malcolm2007GC} prenant en compte les caractéristiques de couleurs et éventuellement de texture des zones à suivre
\item une méthode basée sur le suivi des points d'intérêt sur le visage \cite{saragih2009face}
\newline
\end{itemize}

Il s’agit en particulier d’utiliser des librairies existantes (coupe minimale/flot maximal dans un graphe, suivi de points) et de les adapter à notre problème.

Notre projet se divise donc en l'implémentation de deux programmes pour tester chacune des méthodes. 
Nous avons donc aussi établi les besoins fonctionnels sous-jacents pour les deux méthodes à implémenter.

\subsubsubsection{Méthode basée sur les \textit{Graph Cuts}}
\label{sec:GC}

Les approches de segmentation basées sur les énergies de l'image peuvent \^etre appréhendées comme un problème de graphe. L'image est convertie en une structure de type graphe où la coupe minimale va correspondre à une segmentation optimale de l'image de manière globale.

La méthode de Malcolm et al. \cite{malcolm2007GC} que nous devons tester est une technique de \textit{Graph Cuts} améliorée pour le tracking de un ou plusieurs objets sur une vidéo. 

L'algorithme de \textit{Graph Cuts} est utilisé dans le but d’étiqueter tout les pixels d'une image donnée avec plusieurs labels en fonction de deux paramètres:
\begin{itemize}
  \item le \textit{DataCost} qui sert à déterminer la probabilité qu'un pixel appartienne à chaque label.
  \item le \textit{SmoothnessCost} qui sert à décider de l’attribution d'un label à un pixel en fonction des couleurs de ses voisins.\\
\end{itemize}

Le programme devra calculer pour chaque frame ces deux paramètres pour ensuite appliquer l'algorithme de \textit{Graph Cuts}.

Pour calculer le \textit{DataCost}, nous avons besoin de déterminer les probabilités d'appartenance des pixels à chaque label sur les images prises en entrée.
Notre programme doit donc permettre de calculer sur chaque frame les histogrammes servant de référence pour les zones à reconnaître.

Pour chaque frame nous avons besoin de masques permettant d'identifier les zones témoins (visage, cheveux, fond) pour calculer les histogrammes de référence.
Pour la première frame prise en entrée, le logiciel devra permettre une intéraction avec l'utilisateur pour qu'il définisse lui-même les masques des zones témoins. L'utilisateur devra pouvoir dessiner avec la souris des \textit{scribbles} sur l'image pour chacune des régions à reconnaître.
Un besoin optionnel du programme est de mettre en place une automatisation totale d\`es la premi\`ere frame pour définir les masques.
Sur les frames suivantes, les masques utilisés pour le calcul des histogrammes seront ceux issus du résultat de la segmentation de la frame précédente.

Pour le calcul du \textit{DataCost}, nous avons aussi besoin de calculer des \textit{pénalités} sur les pixels en fonction des régions d'intérêt du visage et des cheveux (susceptibles de bouger dans la vidéo). L'ajout de ce terme dans le calcul du \textit{DataCost} va améliorer la continuité temporelle de la segmentation entre chaque frame successive.
La détermination de ce terme de \textit{pénalité} introduit les besoins suivants pour notre programme:
\begin{itemize}
  \item pour chaque frame des cartes de distances doivent être calculées pour les masques du visage et des cheveux (on attribue à chaque pixel sa distance euclidienne avec le pixel masqué le plus proche).
  \item des calculs de prédictions des localisations des objets d'intérêt (visage et cheveux) doivent être effectués sur chaque frame successive: cette prédiction se base sur la projection du déplacement moyen effectué dans les \textit{n} frames précédentes (on utilise comme localisations les barycentres des objets appelés \"centroides\").
  \item des calculs de retours d'erreurs doivent aussi être effectués en prenant la distance entre les centroïdes prédits des objets et les centroïdes des frames précédentes: le programme devra donc conserver en mémoire les centroïdes des frames précédentes au fil de l'exécution.\\
\end{itemize}
L'utilisateur donnera en entrée du programme: 
\begin{itemize}
\item un paramètre \textbeta{ }pour pondérer l'influence de ce terme de pénalité
\item un paramètre \textgamma{ }  pour limiter le déplacement prédit d'un objet entre deux frames successives à un déplacement maximum fixé
\item un paramètre \textit{n} pour déterminer le nombre de frames précédentes prises en compte pour le calcul du déplacement moyen\\
\end{itemize} 

Le calcul du \textit{SmoothnessCost} utilise le gradient de l'image. Notre programme devra donc aussi calculer les images gradients de chaque frame successive.

Les images en entrée en espace \textit{RGB} devront \^etre converties en espace {YCbCr}. Le \textit{DataCost} sera calculé à partir des composantes \textit{Cb} et \textit{Cr} et le \textit{SmoothnessCost} sera calculé à partir de la composante \textit{Y} de l'image.

Une fois que ces paramètres \textit{DataCost} et \textit{SmoothnessCost} sont calculés sur une frame, l'algorithme de \textit{Graph Cuts} peut être appliqué sur l'image afin de trouver la coupe minimale qui correspondra à une segmentation optimale globale. Notre programme doit donc appliquer un algorithme de \textit{Graph Cuts} sur chaque frame successive de façon optimisée.
Nous utiliserons pour cela la bibliothèque \texttt{GC\_optimization}\footnote{http://www.csd.uwo.ca/faculty/olga/software.html Copyright 2005 Olga Veksler} (cf. ~\ref{sec:existant-GC}).

Le programme doit conserver en mémoire les résultats obtenus en sortie de l'algorithme à chaque frame afin de récupérer à la fin de l'exécution toutes les frames de la vidéo segmentées en sortie. 


\subsubsubsection{Méthode basée sur le suivi des points d'intérêt sur le visage}

Saragih et al. \cite{saragih2009face} proposent une méthode de \textit{face tracking} utilisant un modèle déformable pour suivre les points d'intér\^ets du visage sur des vidéos. La figure \ref{suivi} ci-dessous montre un exemple de résultat obtenu contenant les points d'intér\^et détectés sur un visage.

\begin{figure}[H]
\begin{bigcenter}
  \includegraphics[width=70mm]{img/exemple_suivi.png}
  \caption{Exemple de points d'intér\^et (en vert) obtenus sur un visage (image extraite de l'article de Saragih et al. \cite{saragih2009face})}
\label{suivi}
\end{bigcenter}
\end{figure}

La bibliothèque \texttt{FaceTracker}\footnote{https://github.com/kylemcdonald/FaceTracker} a été développée par Jason Saragih à partir de cette méthode \cite{saragih2009face}. Cette bibliothèque permet de faire du \textit{face tracking} en temps réel de manière optimisée. Elle est écrite en \texttt{C++} et utilise \texttt{OpenCV 2}.

Notre but ici est de voir comment nous pouvons utiliser cette bibliothèque pour l'adapter à notre problème.

Nous devons implémenter un programme qui prend en entrée une vidéo et utilise les outils proposés par \texttt{FaceTracker} pour récupérer les points d'intér\^et du visage sur chaque frame. L'objectif est que le programme donne en sortie un fichier contenant les coordonnées des points d'intér\^et pour chaque frame. 

L'objectif est ensuite d'utiliser ces coordonnées de points d'intér\^et du visage obtenues pour les intégrer à notre problème de segmentation en trois masques: visage, cheveux et fond.

Le premier besoin décidé a été de récupérer les coordonnées issues de la première frame afin de semi-automatiser la segmentation de départ.

Le programme implémenté pour la méthode des \textit{Graph-Cuts} (cf. ~\ref{sec:GC}) peut ainsi prendre en entrée les coordonnées des points d'intér\^et du visage obtenues sur la première frame pour calculer l'histogramme de référence de la région du visage. L'utilisateur doit alors dessiner des \textit{scribbles} pour les régions des cheveux et du fond pour segmenter la première frame. 

Cette utilisation des coordonnées des points d'intér\^et du visage permet dans un premier temps d'automatiser un peu plus le travail de segmentation sur la première frame. Elle permet aussi d'assurer une première segmentation robuste du visage en prenant pour calculer l'histogramme de référence les points caractéristiques nécessaires à la réussite de la segmentation (yeux, bouche, peau).


\subsection{Faisabilit\'e et risques}

La méthode des \textit{Graph Cuts} que nous allons implémenter se base seulement sur des informations de couleurs. Elle fonctionnera donc bien sur des images où les régions sont homogènes et distinctes au niveau des couleurs (e.g. fond homogène blanc, v\^etement bleu (cf. Figure \ref{belle-seg})). Le risque majeur est d'obtenir de mauvais résultats sur des images où le fond est très hétérogène et où les régions ont des informations de couleurs proches (e.g. cheveux, v\^etements et éléments du fond de couleurs semblables (cf. Figure \ref{Raph})). Il faudra alors utiliser des informations supplémentaires comme les caractéristiques de texture des zones à suivre.

La réussite de la segmentation risque aussi d'etre altérée si la personne ciblée porte des lunettes, une barbe ou des tatouages sur le visage et le cou. Le risque serait par exemple que les parties du visage occludées par des lunettes ou des tatouages ne soient pas étiquettées comme appartenant au visage et que la bouche entourée par de la barbe soit étiquettée comme des cheveux.

Dans le cas de la segmentation sur un seul visage, l'apparition d'autres visages sur la vidéo devant etre étiquettés comme appartenant au fond peuvent aussi pertuber la segmentation.

\begin{figure}[H]
\begin{bigcenter}
  \includegraphics[width=70mm]{img/belle-seg.jpeg}
  \caption{Exemple de fond homogène blanc avec un v\^etement bleu}
\label{belle-seg}
\end{bigcenter}
\end{figure}

\begin{figure}[H]
\begin{bigcenter}
\includegraphics[width=70mm]{img/Raph}
\caption{Exemple de cheveux, v\^etements et éléments du fond de couleurs noires proches}
\label{Raph}
\end{bigcenter}
\end{figure}



\subsection{Besoins non fonctionnels}

Nous allons maintenant \'etudier quels sont les besoins non fonctionnels de notre projet.

\subsubsection{Temps d'\'ex\'ecution}

Notre application est destinée à \^etre utilisée sur des vidéos relativement longues (de l'ordre de la vingtaine de minutes jusqu'à deux heures, pour les plus longues) de résolution de \textit{720 * 576}. De ce fait, le temps d'exécution doit \^etre raisonnable : idéalement de l'ordre de la seconde pour chaque frame, au maximum dix secondes.

\subsubsection{Version et portabilit\'e}

Notre programme est tout d'abord r\'ealis\'e sous \texttt{MatLab}, avec des intéractions possibles avec le langage \texttt{C} via l'API \texttt{Mex}. La version de \texttt{MatLab} que nous utiliserons est la \textit{2009b}. Notre code \texttt{MatLab} doit \^etre portable sur les systèmes d'exploitation \texttt{Windows}, \texttt{Unix} et \texttt{Mac OS}. Si nous utilisons un interfaçage avec le langage \texttt{C}, nous viserons à rester le plus portable possible.

\subsubsection{Ergonomie}

Notre logiciel n'a pas besoin en soi d'une interface graphique. N\'eanmoins, au lancement du programme, il est envisageable qu'une bo\^ite de dialogue s'ouvre pour demander \`a l'utilisateur de faire le traitement sur la premi\`ere frame. L'interface sera simple, mais lisible et facile d'acc\`es pour ces traitements en attendant une automatisation compl\`ete.

\subsection{Tests}

Dans cette partie, nous d\'ecrivons quelques tests auxquels nous avons r\'efl\'echi, dans notre analyse des besoins.

\subsubsection{Qualité de la segmentation}

Pour évaluer les qualités des segmentations obtenues, nous pouvons effectuer des tests en suivant le protocole suivant :
\begin{itemize}
\item Nous prenons la 1è frame de la vidéo originale, et toute frame par pas de 20 (par exemple, sur une vidéo de 90 frames, nous prenons les frames 1, 21, 41, 61 et 81).
\item Pour chacune de ces frames, nous effectuons une segmentation manuelle (avec un outil de dessin comme \texttt{Paint}) pour avoir un résultat précis.
\item Puis nous extrayons les mêmes frames sur la vidéo de sortie de notre application.
\item Nous effectuons alors une comparaison (différence) entre les deux images : les pixels communs (considérés comme bon) sont en blancs et les pixels différents (considérés comme mauvais) en noirs. Nous calculons ainsi les taux de réussite au cours du temps.
\end{itemize}

Il faudrait effectuer ce test sur des vidéos contenant chacune une des situations possibles énoncées dans les besoins où la segmentation doit rester robuste:
\begin{itemize}
\item un visage totalement fixe
\item un visage fixe avec changements d'expression du visage
\item un visage en mouvement (déplacement dans la scène)
\item occlusion partielle d'un visage fixe
\item changement d'illumination dans la scène
\item plusieurs visages dans la scènes (fixes et en mouvement)
\end{itemize}

\subsubsection{Performances}

Nous pourrions aussi faire des tests de stress sur le programme en lui donnant en entrée des images de hautes résolutions (e.g. \textit{1920 * 1080}) pour voir si le programme réussit à tourner et mesurer le temps d'exécution mais ce n'est pas la priorité.
