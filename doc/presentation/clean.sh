#!/bin/sh

find . -name "*.out" -delete -print
find . -name "*.aux" -delete -print
find . -name "*.nav" -delete -print
find . -name "*.snm" -delete -print
find . -name "*.toc" -delete -print
find . -name "*.bbl" -delete -print
find . -name "*.blg" -delete -print
find . -name "*.log" -delete -print
find . -name "*~" -delete -print
