# LISEZ-MOI #

Ce projet fait en MATLAB/C a pour objectif de permettre le suivi de visages et des cheveux d'individus dans des vidéos. Nous utilisons une méthode de Graph-Cut afin de pouvoir définir un masque de segmentation. Ledit masque est composé en trois régions :

1. visage
2. cheveux
3. fond

Ces masques peuvent entre autres servir à "cacher" ou censurer des éléments sensibles dans une vidéo.

### Structure du projet ###

- code/ contient les scripts et fonctions ainsi que les dépendances (bibliothèques etc.)
- data/ contient les fichiers binaires, vidéos et images, utilisés lors des tests
- utils/ contient des scripts shell (e.g. un script d'extraction des images d'une vidéo)

### Dépendances ###

Le projet est développé sous un environnement Linux.

Le projet est prévu pour fonctionner avec la version 2009b de Matlab.

L'application utilise les fonctions de Graph-Cut définies dans la bibliothèque GC. Étant en partie codée avec un interfaçage MEX, il est nécessaire de la compiler avant de pouvoir exécuter le programme. Pour cela il existe un script 'compile_gc.m' permettant de faire la compilation.

L'application ne prends pas directement une vidéo en entrée mais un ensemble d'images. Les images doivent être extraites, au préalable, dans un dossier. Il faudra ensuite passer le chemin vers le dossier contenant toutes les images pour enfin lancer le programme.

### Déroulement de l'application ###

L'application prend en entrée un ensemble d'images (vidéo) et retourne un ensemble d'images correspondant aux masques, cités ci-dessus, le tout sous forme d'une vidéo AVI.

L'utilisateur aura la possibilité de modifier les paramètres nécessaires à la segmentation via Graph-Cut afin de l'ajuster/adapter à ses besoins. Il pourra aussi soit définir trois régions manuellement ou utiliser des masques existants, en les chargeant lors de l'exécution.
Une fois la première image segmentée, l'utilisateur pourra dès lors confirmer et continuer l'exécution, ou bien avorter l'exécution pour modifier certains paramètres etc.

Une fois toutes les images segmentées, elles sont sauvegardées sous un format vidéo en AVI à un endroit spécifié au début du programme par l'utilisateur.

Enfin, l'utilisateur pourra, s'il le souhaite, sauvegarder les 'scribbles' ou les masques utilisés lors de la première segmentation, pour par exemple les réutiliser etc.

#### Exemple ####

Cela permet d'extraire les images d'une vidéo et de les stocker dans un dossier : _< nom de la vidéo >

```sh
$ cd data
$ ../utils/extract-frames.sh sample1.mpeg
```

Voici un exemple simple d'utilisation :

```
cd code;
addpath('GC/');

inputDir='../data/_sample1.mpeg';
outputFile='../data/outSample1.avi';

N=3;
beta=3;
gamma=5;

gc_params=struct('lambda', 20, 'num_bins', 16, 'max_cost', 1000, 'default', [], 'scribbles', [], 'face_landmarks', [], 'box', []);

[mov, ~]=create_mask_movie(inputDir, outputFile, gc_params, beta, gamma, N);
```
Toutes les fonctions utlisées disposent de commentaires d'aide et expliquent le rôle des variables présentées dans l'exemple ci-dessus.

### Face Tracker ###

FaceTracker est une bibliothèque écrite en C/C++ permettant de détecter et afficher les points d'intérêts d'un visage. Un exemple d'utilisation est présent dans la bibliothèque 'FaceTracker/src/exe/face_tracker.cc', et un fichier Makefile est livré avec.

Il prend un flux provenant d'une webcam et lance une détection. Pour le bien de notre programme nous avons fait en sorte de pouvoir lui passer en entrée une vidéo, ainsi que la possibilité de sauvegarder les points d'intérêt de la première image afin de les utiliser pour détecter le visage lors de la segmentation via Graph-Cut.

Cet exemple permet de sauvegarder les points d'intérêts d'un visage présent dans une vidéo :

```sh
$ ./face_tracker -i sample.mpeg -o points.txt
```

L'utilisateur peut alors faire tourner une première fois cette bibliothèque avant de lancer notre programme.

Nous avons aussi développé un interfaçage MEX afin d'éviter à l'utilisateur d'avoir à exécuter deux programmes différents. Néanmoins, n'ayant pas eu le temps de l'intégrer et d'effectuer tous les tests sur cette interface, elle reste sur le plan expérimentale et n'est pas directement utilisée dans le programme principal.


### Idées d'amélioration ###

L'utilisateur doit passer par le script 'test_mask_movie.m', qui se trouve être le script principal, pour pouvoir exécuter le programme. Il contient toutes les abstractions utiles et simplifie l'utilisation de l'algorithme de Graph-Cut ; transformer ce script en fonction permettrait une configuration plus intuitive.

L'interfaçage MEX de FaceTracker a son intérêt : terminer et intégrer cette fonction améliorerait l'expérience de l'utilisateur, sans avoir à passer par un fichier intermédiaire.
