#!/bin/sh

if [ -z "$1" ]; then
	echo "Usage: $0 <filename>"
	exit 1
else
	IN_FILENAME=$1
	OUT_DIR=_$IN_FILENAME

	if [ ! -d "$OUT_DIR" ]; then
		mkdir $OUT_DIR
	fi

	avconv -i $IN_FILENAME -f image2 $OUT_DIR/image-%05d.png

	exit 0
fi