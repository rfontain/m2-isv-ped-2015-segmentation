#!/bin/sh

if [ -z "$1" ]; then
	echo "Usage: $0 <filename>"
	echo "Usage: $0 <filename> <output_filename>"
	exit 1
else
	IN_FILENAME=$1
	OUT_FILENAME=$2

	if [ -z "$OUT_FILENAME" ]; then
		OUT_FILENAME=720x576-$IN_FILENAME
	fi

	avconv -i $IN_FILENAME -s 720x576 $OUT_FILENAME

	echo "\n"
	echo "Video resized and saved at \"$OUT_FILENAME\"."

	exit 0
fi