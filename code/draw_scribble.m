function [ mask ] = draw_scribble(I )

disp('Press any key to draw scribbles - Press + to enlarge the scribbles and - to reduce it; press q when done');    % Affichage image en niveau de gris.
figure; imshow(I);
hold on

scribble_size=2;
mask=zeros(size(I));
setappdata(gcf, 'scribble_size', scribble_size);
while true
    stopit = 1;
    setappdata(gcf, 'x', []);
    setappdata(gcf, 'y', []);
    setappdata(gcf, 'mask',mask);   
    setappdata(gcf, 'stopit', stopit);
    
            disp('press a key :Press + to enlarge the scribbles and - to reduce it; press q when done; any other key to draw scribbles')
    w = waitforbuttonpress; %quit with ctrl
    if w ~= 0
       
        kkey = get(gcf,'CurrentCharacter');
        if kkey == '+'
            scribble_size = scribble_size + 1;
            waitfor(msgbox(['scribble_size du scrible = ', num2str(scribble_size * 2 +1)],'scribble_size','none'));
            % stopit=1;
        elseif  kkey == '-'
            if scribble_size <=0
                % pblm (résolu)
                waitfor(msgbox('scribble_size du scrible trop petite. ','scribble_size','error'));
                scribble_size = 1;
                % stopit=1;
            else
                scribble_size = scribble_size - 1;
                waitfor(msgbox(['scribble_size du scrible = ', num2str(scribble_size * 2 +1)],'scribble_size','none'));
                % stopit=1;
            end
        elseif  kkey == 'q'
            close(1)
            return
        end
        % else %if kkey == 'd'
            stopit=0;
            drawnow;
            disp('draw a scrible')
            % set(gcf, 'windowbuttonmotionfcn', '');
            % set(gcf, 'WindowButtonDownFcn',  '');
            % set(gcf, 'WindowButtonUpFcn',   '');
        % end
    end
    
    
    setappdata(gcf,'stopit',stopit) ;
    
    setappdata(gcf,'scribble_size',scribble_size) ;
    setappdata(gcf, 'x', []);
    setappdata(gcf, 'y', []);
    while (stopit==0)
        set(gcf,'Pointer','hand') ;
        drawnow
        set(gcf, 'WindowButtonDownFcn',  @buttonDown);
        %
        
        set(gcf, 'WindowButtonUpFcn',   @buttonUp);
        
        stopit = getappdata(gcf, 'stopit');
        
    end
    
    
    mask= getappdata(gcf, 'mask');
end
%figure; imshow(im2double(mask));
%figure; imshow(double(mask));
end

function buttonDown(varargin)
    set(gcf, 'WindowButtonMotionFcn', @onScrible);
    drawnow
end

function onScrible(varargin)
    x= getappdata(gcf, 'x');
    y= getappdata(gcf, 'y');

    pt_courant= get(gca,'currentpoint');
    setappdata( gcf, 'x' , [x, pt_courant(1,1)]);
    setappdata( gcf, 'y' , [y, pt_courant(1,2)]);
    scribble_size = getappdata(gcf, 'scribble_size');
    mask= getappdata(gcf, 'mask');
    [H, W, ~] = size(mask);
    for j=pt_courant(1,1)-scribble_size:pt_courant(1,1)+scribble_size
        for i=pt_courant(1,2)-scribble_size:pt_courant(1,2)+scribble_size
            if i>0 && i<=H && j>0 && j<=W

                plot(j, i, 'g.');
                mask(uint16(i),uint16(j)) =255;
            end
        end
    end

    setappdata( gcf, 'mask' , mask);
    drawnow
end

function buttonUp(varargin)
    setappdata( gcf, 'stopit' , 1);
    set(gcf, 'windowbuttonmotionfcn', '');
end


