function [ v_cost, h_cost ] = get_smoothness_costs( im_YCbCr, lambda )
%GET_WEIGHTS Calculate the vertical and horizontal costs for the SmoothnessCosts.
% Usage:
%     [v_cost, h_cost] = get_smoothness_costs(im_YCbCr, lambda)
%
% Inputs:
% - im_YCbCr: double matrix of an image in YCbCr color space
% - lambda:   lambda >= 0, weight for relative influence
%
% Outputs:
% - v_cost: vertical cost as a im_YCbCr sized matrix
% - h_cost: horizontal cost as a im_YCbCr sized matrix

[H, W, ~] = size(im_YCbCr);

Y = im_YCbCr(:,:,1);

vert = zeros(H, W);
vert(1:end-1,:) = Y(1:end-1,:) - Y(2:end,:);
vert = vert .^ 2;

horiz = zeros(H, W);
horiz(:,1:end-1) = Y(:,1:end-1) - Y(:,2:end);
horiz = horiz .^2;

vert_size  = numel(vert);
horiz_size = numel(horiz);

sigma = 2 * (sum(horiz(:)) + sum(vert(:))) / (vert_size + horiz_size);

v_cost = lambda * exp(-vert  / sigma);
h_cost = lambda * exp(-horiz / sigma);

end
