#!/bin/sh
# Run test_FaceTrackerMex with matlab -nodesktop
#   in order to be able to compile and execute FaceTrackerMex.cc
#   we need to locate libstdc++ in the system
#   use `locate libstdc++.so` in a shell
#   in this example we have: /usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.19
#   then the command to launch matlab is:
#       LD_PRELOAD=$(path_to_libstdc++) matlab -nodesktop -r (statement)

# clean last build
find . -name "*.mexa*" -delete
find . -name "*.o" -delete
find . -name "*.d" -delete

LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6 matlab -nodesktop -r test_FaceTrackerMex
