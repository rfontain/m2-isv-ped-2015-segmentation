
function Y_Graphcut_Gaussiennes()
% An example of how to segment a color image according to pixel colors.
% Fisrt stage identifies k distinct clusters in the color space of the
% image. Then the image is segmented according to these regions; each pixel
% is assigned to its cluster and the GraphCut poses smoothness constraint
% on this labeling.

separation='----------------------------------------';

close all
addpath('../GC/');
% read an image
im =im2double(rgb2ycbcr(imread('ID01_001.bmp')));
im2 = im2double(imread('ID01_001_objet.bmp'));
im3 = im2double(imread('ID01_001_cheveux.bmp'));
im4=  im2double(imread('ID01_001_fond.bmp'));
ref= im2double(rgb2gray(imread('ID01_001_ref.bmp')));


% normalize input data
tmp1=(ref>0 );
tmp2=(ref<1 );
zoneCheveux=tmp1==tmp2;
zoneFond=(ref==0);
zoneVisage=(ref==1);
ref(zoneCheveux)=0.5;

%Parameter initialization
lambdaListe=[0 50 100 150 200 250];
fin=size(lambdaListe);
fin=fin(1)*fin(2);
for i=1:fin
lambda=lambdaListe(i);
lambda   %print parameter
[imax jmax]=size(im(:,:,1)); 



imY=im(:,:,1);
imCB=im(:,:,2);
imCR=im(:,:,3);
objet=im2(:,:,1)==1;
cheveux=im3(:,:,1)==1;
fond=im4(:,:,1)==1;


x=zeros(imax,jmax); 
x(1:end-1,:)=imY(1:end-1,:)-imY(2:end,:); 
x=(x.^2);
sizetmp=size(x);
sizetmp=sizetmp(1)*sizetmp(2);
sigma=(sum(x(:))/sizetmp)*2;
vC=lambda*exp(-x/sigma);
 
x=zeros(imax,jmax);                           
x(:,1:end-1)=imY(:,1:end-1)-imY(:,2:end);
x=(x.^2);
sizetmp=size(x);
sizetmp=sizetmp(1)*sizetmp(2);
sigma=(sum(x(:))/sizetmp)*2;
hC=lambda*exp(-x/sigma);           



prevLabel=ref;

K=5; % iterations for Kmeans in CalcLogPLikelihood


fgMean = [];
cvMean = [];
bgMean = [];

    bgIds   = find(prevLabel == 0);
    fgIds   = find(prevLabel == 1);
    cvIds   = find(prevLabel == 0.5);
    
    %%% Use NOT FIXED labels to get the Log Probability Likelihood 
    %%% of the pixels to a GMM color model (inferred from the labels...)
    bgMeanInit = bgMean;
    fgMeanInit = fgMean;
    cvMeanInit = cvMean ;
    [bgLogPL fgLogPL cvLogPL bgMean fgMean cvMean ] =  CalcLogPLikelihood(im, K, bgIds,fgIds,cvIds, bgMeanInit, fgMeanInit,cvMeanInit );
    
    %%% Use our A-Priori knowledge of Background labels & set the Forground
    %%% weights according to it.
    %fgLogPL(fixedBG) = max(max(fgLogPL));
        
    %%% Now that we have all inputs, calculate the min-cut of the graph
    %%% using Bagon's code. Not much to explain here, for more details read
    %%% the graph cut documentation in the   GraphCut.m    file.
    
    
   % scrible's pixels atribution 
    bgLogPL(cheveux)=10000;
    cvLogPL(fond)=10000;
    fgLogPL(cheveux)=10000;
    bgLogPL(objet)=10000;
    cvLogPL(objet)=10000;
    fgLogPL(fond)=10000;
    dc =cat(3 ,fgLogPL,cvLogPL, bgLogPL);



Sc=ones(3)-eye(3);
gch = GraphCut('open', dc ,Sc,vC,hC);
[gch L] = GraphCut('expand',gch);
gch = GraphCut('close', gch);



% show results
zone1=L==0;
zone2=L==1;



zone2=zone2/2;


res=zone1+zone2;

x=res==ref;


imshow(res);

tmp=imhist(x,2);
nombrepixok=tmp(2);
y=size(res);
nombrepixtot=y(1)*y(2);
reussite=(nombrepixok/nombrepixtot)*100;
reussite


tmp=imhist(res(zoneVisage),3);
pixelzone=sum(zoneVisage(:));
cheveuxDansVisage=(tmp(2)/pixelzone)*100;
fondDansVisage=(tmp(1)/pixelzone)*100;
cheveuxDansVisage
fondDansVisage


tmp=imhist(res(zoneCheveux),3);
pixelzone=sum(zoneCheveux(:));
visageDansCheveux=(tmp(3)/pixelzone)*100;
fondDansCheveux=(tmp(1)/pixelzone)*100;
visageDansCheveux
fondDansCheveux


tmp=imhist(res(zoneFond),3);
pixelzone=sum(zoneFond(:));
visageDansFond=(tmp(3)/pixelzone)*100;
cheveuxDansFond=(tmp(2)/pixelzone)*100;
visageDansFond
cheveuxDansFond

separation

end






function [ bgLogPL fgLogPL cvLogPL bgMean fgMean cvMean ] = CalcLogPLikelihood(im, K, bgIds,fgIds,cvIds , bgMeanInit, fgMeanInit,cvMeanInit )

numPixels = size(im,1) * size(im,2);
allBGLogPL = zeros(numPixels,K);
allFGLogPL = zeros(numPixels,K);
allCVLogPL = zeros(numPixels,K);

%%% Seperate color channels 
R = im(:,:,1);
G = im(:,:,2);
B = im(:,:,3);

%%% Prepare the color datasets according to the input labels 
imageValues = [R(:) G(:) B(:)];
bgValues = [R(bgIds)    G(bgIds)     B(bgIds)];
fgValues = [R(fgIds)    G(fgIds)     B(fgIds)];
cvValues = [R(cvIds)    G(cvIds)     B(cvIds)];
numBGValues = size(bgValues,1);
numFGValues = size(fgValues,1);
numCVValues = size(cvValues,1);

%%%%%%
% Use a 'manual' way to calculate the GMM parameters, instead of using
% Matlab's gmdistribution.fit() function. This is due to better speed 
% results..
% Start with Kmeans centroids calculation :
opts = statset('kmeans');
opts.MaxIter = 200;

if ( ~isempty(bgMeanInit) && ~isempty(fgMeanInit) && ~isempty(cvMeanInit))
    [bgClusterIds bgMean] = kmeans(bgValues, K, 'start', bgMeanInit,  'emptyaction','singleton' ,'Options',opts);
    [fgClusterIds fgMean] = kmeans(fgValues, K, 'start', fgMeanInit,  'emptyaction','singleton', 'Options',opts);
    [cvClusterIds cvMean] = kmeans(cvValues, K, 'start', cvMeanInit,  'emptyaction','singleton', 'Options',opts);
else
    [bgClusterIds bgMean] = kmeans(bgValues, K, 'emptyaction','singleton' ,'Options',opts);
    [fgClusterIds fgMean] = kmeans(fgValues, K, 'emptyaction','singleton', 'Options',opts);
    [cvClusterIds cvMean] = kmeans(cvValues, K, 'emptyaction','singleton', 'Options',opts);
end

checkSumFG = 0;
checkSumBG = 0;
checkSumCV = 0;

for k=1:K
    %%% Get the k Gaussian weights for Background & Forground 
    bgGaussianWeight = nnz(bgClusterIds==k)/numBGValues;
    fgGaussianWeight = nnz(fgClusterIds==k)/numFGValues;
    cvGaussianWeight = nnz(cvClusterIds==k)/numCVValues;
    
    checkSumBG = checkSumBG + bgGaussianWeight;
    checkSumFG = checkSumFG + fgGaussianWeight;
    checkSumCV = checkSumCV + cvGaussianWeight;

    %%% FOR ALL PIXELS - calculate the distance from the k gaussian (BG & FG)
    bgDist = imageValues - repmat(bgMean(k,:),size(imageValues,1),1);
    fgDist = imageValues - repmat(fgMean(k,:),size(imageValues,1),1);
    cvDist = imageValues - repmat(cvMean(k,:),size(imageValues,1),1);

    %%% Calculate the gaussian covariance matrix & use it to calculate
    %%% all of the pixels likelihood to it :
    bgCovarianceMat = cov(bgValues(bgClusterIds==k,:));
    fgCovarianceMat = cov(fgValues(fgClusterIds==k,:));
    cvCovarianceMat = cov(cvValues(cvClusterIds==k,:));
    allBGLogPL(:,k) = -log(bgGaussianWeight)+0.5*log(det(bgCovarianceMat)) + 0.5*sum( (bgDist/bgCovarianceMat).*bgDist, 2 );
    allFGLogPL(:,k) = -log(fgGaussianWeight)+0.5*log(det(fgCovarianceMat)) + 0.5*sum( (fgDist/fgCovarianceMat).*fgDist, 2 );
    allCVLogPL(:,k) = -log(cvGaussianWeight)+0.5*log(det(cvCovarianceMat)) + 0.5*sum( (cvDist/cvCovarianceMat).*cvDist, 2 );
end



assert(abs(checkSumBG - 1) < 1e-6 && abs(checkSumFG - 1)  < 1e-6  && abs(checkSumCV - 1)  < 1e-6);

%%% Last, as seen in the GrabCut paper, take the minimum Log likelihood
%%% (    argmin(Dn)    )
bgLogPL = reshape(min(allBGLogPL, [], 2),size(im,1), size(im,2));
fgLogPL = reshape(min(allFGLogPL, [], 2),size(im,1), size(im,2));
cvLogPL = reshape(min(allCVLogPL, [], 2),size(im,1), size(im,2));



