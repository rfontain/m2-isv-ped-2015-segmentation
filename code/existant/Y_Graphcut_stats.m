function Y_Graphcut_stats()
% An example of how to segment a color image according to pixel colors.
% Fisrt stage identifies k distinct clusters in the color space of the
% image. Then the image is segmented according to these regions; each pixel
% is assigned to its cluster and the GraphCut poses smoothness constraint
% on this labeling.

separation='----------------------------------------';

close all;
clc;
addpath('../GC/');
% read an image
%im = im2double(rgb2ycbcr(imread('Portrait-test.jpg')));
im = im2double(rgb2ycbcr(imread('../../data/stage/ID02_001.bmp')));
im2 = im2double(imread('../../data/stage/ID02_001_objet.bmp'));
im3 = im2double(imread('../../data/stage/ID02_001_cheveux.bmp'));
im4=  im2double(imread('../../data/stage/ID02_001_fond.bmp'));
ref= im2double(rgb2gray(imread('../../data/stage/ID02_001_ref.bmp')));
imshow(ref);
bidule=(ref>0 );
bidule2=(ref<1 );
zoneCheveux=bidule==bidule2;
zoneFond=(ref==0);
zoneVisage=(ref==1);
ref(zoneCheveux)=0.5;


lambdaListe=[0 50 100 150 200 250];
fin=size(lambdaListe);
fin=fin(1)*fin(2);
for i=1:fin

lambda=lambdaListe(i);
lambda
[imax jmax]=size(im(:,:,1)); 
 

imY=im(:,:,1);
imCB=im(:,:,2);
imCR=im(:,:,3);


objet=im2(:,:,1)==1;

cheveux=im3(:,:,1)==1;

fond=im4(:,:,1)==1;





 
 histoObjetR=imhist(imY(objet),16);
 histoObjetG=imhist(imCB(objet),16);
 histoObjetB=imhist(imCR(objet),16);
 histoCheveuxR=imhist(imY(cheveux),16);
 histoCheveuxG=imhist(imCB(cheveux),16);
 histoCheveuxB=imhist(imCR(cheveux),16);
 histoFondR=imhist(imY(fond),16);
 histoFondG=imhist(imCB(fond),16);
 histoFondB=imhist(imCR(fond),16);
 



x=zeros(imax,jmax); 
x(1:end-1,:)=imY(1:end-1,:)-imY(2:end,:); 
x=(x.^2);
sizetmp=size(x);
sizetmp=sizetmp(1)*sizetmp(2);
sigma=(sum(x(:))/sizetmp)*2;
vC=lambda*exp(-x/sigma);
 
x=zeros(imax,jmax);                           
x(:,1:end-1)=imY(:,1:end-1)-imY(:,2:end);
x=(x.^2);
sizetmp=size(x);
sizetmp=sizetmp(1)*sizetmp(2);
sigma=(sum(x(:))/sizetmp)*2;
hC=lambda*exp(-x/sigma);           



histoObjetR=histoObjetR/sum(histoObjetR);
histoCheveuxR=histoCheveuxR/sum(histoCheveuxR);
histoFondR=histoFondR/sum(histoFondR);

histoObjetG=histoObjetG/sum(histoObjetG);
histoCheveuxG=histoCheveuxG/sum(histoCheveuxG);
histoFondG=histoFondG/sum(histoFondG);

histoObjetB=histoObjetB/sum(histoObjetB);
histoCheveuxB=histoCheveuxB/sum(histoCheveuxB);
histoFondB=histoFondB/sum(histoFondB);

histoObjetR=histoObjetR+0.0001;
histoCheveuxR=histoCheveuxR+0.0001;
histoFondR=histoFondR+0.0001;

histoObjetG=histoObjetG+0.0001;
histoCheveuxG=histoCheveuxG+0.0001;
histoFondG=histoFondG+0.0001;

histoObjetB=histoObjetB+0.0001;
histoCheveuxB=histoCheveuxB+0.0001;
histoFondB=histoFondB+0.0001;



DataCost(imax,jmax,3)=0;

pixelR=int64(im(:,:,1)*15+1);
pixelG=int64(imCB(:,:)*15+1);
pixelB=int64(imCR(:,:)*15+1);

probaOR=histoObjetR(pixelR);
probaOG=histoObjetG(pixelG);
probaOB=histoObjetB(pixelB);

probaO=probaOG.*probaOB.*probaOR;


probaCR=histoCheveuxR(pixelR);
probaCG=histoCheveuxG(pixelG);
probaCB=histoCheveuxB(pixelB);

probaC=probaCG.*probaCB.*probaCR;


probaFR=histoFondR(pixelR);
probaFG=histoFondG(pixelG);
probaFB=histoFondB(pixelB);

probaF=probaFG.*probaFB.*probaFR;


d1=-log(probaO);
d2=-log(probaC);
d3=-log(probaF);

d1(cheveux)=10000;
d1(fond)=10000;
d2(objet)=10000;
d2(fond)=10000;
d3(objet)=10000;
d3(cheveux)=10000;


DataCost(:,:,1)=d1;
DataCost(:,:,2)=d2;
DataCost(:,:,3)=d3;




Sc=ones(3)-eye(3);
gch = GraphCut('open', DataCost, Sc, vC, hC);
[gch L] = GraphCut('expand', gch);
gch = GraphCut('close', gch);



% show results
zone1=L==0;
zone2=L==1;



zone2=zone2/2;


res=zone1+zone2;

x=res==ref;


imshow(res);

tmp=imhist(x,2);
nombrepixok=tmp(2);
y=size(res);
nombrepixtot=y(1)*y(2);
reussite=(nombrepixok/nombrepixtot)*100;
reussite


tmp=imhist(res(zoneVisage),3);
pixelzone=sum(zoneVisage(:));
cheveuxDansVisage=(tmp(2)/pixelzone)*100;
fondDansVisage=(tmp(1)/pixelzone)*100;
cheveuxDansVisage
fondDansVisage


tmp=imhist(res(zoneCheveux),3);
pixelzone=sum(zoneCheveux(:));
visageDansCheveux=(tmp(3)/pixelzone)*100;
fondDansCheveux=(tmp(1)/pixelzone)*100;
visageDansCheveux
fondDansCheveux


tmp=imhist(res(zoneFond),3);
pixelzone=sum(zoneFond(:));
visageDansFond=(tmp(3)/pixelzone)*100;
cheveuxDansFond=(tmp(2)/pixelzone)*100;
visageDansFond
cheveuxDansFond

separation

end









