#include <mex.h>
#include <matrix.h>

#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>

#include <FaceTracker/Tracker.h>

// Usage msg
#define USAGE_MSG "Usage:\n\t[points, status] = FaceTrackerMex(im, ftFile, conFile, triFile, fcheck);\n\nInputs:\n- im:      an image\n- ftFile:  tracker model filename\n- conFile: connectivity model filename\n- triFile: triangulation model filename\n- fcheck:  boolean, check for failure"

// Input arguments
#define IM      prhs[0]
#define FTFILE  prhs[1]
#define CONFILE prhs[2]
#define TRIFILE prhs[3]
#define FCHECK  prhs[4]

#define REQUIRED_NRHS 5

// Outoput arguments
#define POINTS  plhs[0]
#define STATUS  plhs[1]

#define REQUIRED_NLHS 1

// Usage conditions
#define MX_CHECK_INTEGER(ARRAY)\
  (mxGetNumberOfElements(ARRAY) == 1)

#define USAGE_CONDITIONS nrhs < REQUIRED_NRHS || nlhs < REQUIRED_NLHS\
 || !mxIsChar(FTFILE) || !mxIsChar(CONFILE) || !mxIsChar(TRIFILE)\
 || !MX_CHECK_INTEGER(FCHECK)

// Type definition
typedef char int8;
typedef unsigned char uint8;

// Output function definition
static inline void SavePoints(uint8 *pts, cv::Mat &shape, cv::Mat &visi);

/**FACETRACKERMEX MEX interface for OpenCV/FaceTracker libraries.
*  Usage:
*    points = FaceTrakerMex(im, ftFile, conFile, triFile, fcheck);
*
*  Inputs:
*  - im:      an image
*  - ftFile:  tracker model filename
*  - conFile: connectivity model filename
*  - triFile: triangulation model filename
*  - fcheck:  boolean, check for failure
*
*  Outputs:
*  - points:  array of 2D-double points returned by the FaceTracker library
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if(USAGE_CONDITIONS)
    mexErrMsgIdAndTxt("FaceTrackerMex:usage", USAGE_MSG);

  /* check image elements class (must be uint8) */
  mxClassID imClass = mxGetClassID(IM);

  if(imClass != mxUINT8_CLASS)
    mexErrMsgIdAndTxt("FaceTrackerMex:imClass",
      "Image's elements must be type of uint8.");

  /* check image matrix dimensions */
  const mwSize *dims = mxGetDimensions(IM);
  mwSize nofDims = mxGetNumberOfDimensions(IM);

  if(nofDims < 2)
    mexErrMsgIdAndTxt("FaceTrackerMex:nofDims", "Input image matrix must be at least a 2D matrix.");

  /* get image matrix dimensions */
  size_t H = (size_t) dims[0];
  size_t W = (size_t) dims[1];
  size_t C = nofDims > 2 ? (size_t) dims[2] : 2;

  if(C < 2 || C > 3)
    mexErrMsgIdAndTxt("FaceTrackerMex:nofDims", "Input image matrix must be a 2D or 3D matrix.");

  /* associate C with CV_8UC1 or CV_8UC3 type */
  if(C == 2) // then we use CV_8UC1 (grayscale)
    C = 1;
  // else we use CV_8UC3 (color)

  /* associate inputs */
  uint8 *im      = (uint8 *) mxGetPr(IM);
  char  *ftFile  = (char *)  mxArrayToString(FTFILE);
  char  *conFile = (char *)  mxArrayToString(CONFILE);
  char  *triFile = (char *)  mxArrayToString(TRIFILE);
  bool  fcheck   = int(mxGetScalar(FCHECK)) != 0;

  /* create opencv mat */
  cv::Mat *frame = 0;

  if(C == 3)
    frame = new cv::Mat(H, W, CV_8UC3); // grayscale
  else
    frame = new cv::Mat(H, W, CV_8UC1); // color

  if(!frame)
    mexErrMsgIdAndTxt("FaceTrackerMex:frame", "Unable to allocate memory for cv::Mat *frame");

  /* fill frame matrix with im data */
  // TODO: optimization about imbricated loops
  if(C == 3)
    for(size_t x = 0; x < W; ++x)
      for(size_t y = 0; y < H; ++y)
        for(size_t c = 0; c < C; ++c)
        {
          size_t idIm = x + H * (y + H * c);
          frame->at<cv::Vec3b>(y, x)[c] = im[idIm];
        }
  else
    for(size_t x = 0; x < W; ++x)
      for(size_t y = 0; y < H; ++y)
      {
        size_t idIm = x + y * H;
        frame->at<uint8>(y, x) = im[idIm];
      }

  /* prepare execution of FaceTracker lib */
  // set other tracking parameters
  std::vector<int> wSize1 { 7 };
  std::vector<int> wSize2 { 11, 9, 7 };
  size_t nIter = 5;
  double clamp = 3;
  double fTol  = .01;
  int fpd = -1;

  FACETRACKER::Tracker model(ftFile);

  cv::Mat tri = FACETRACKER::IO::LoadTri(triFile);
  cv::Mat con = FACETRACKER::IO::LoadCon(conFile);

  // initialize grayscaled frame
  cv::Mat gray(H, W, CV_8UC1);

  if(C == 3)
  {
    cv::cvtColor(*frame, gray, CV_RGB2GRAY);
    // FIXME:
    std::cout << "It seems that cv::cvtColor(src, dst, CV_RGB2GRAY) does not work as espected. ";
    std::cout << "Try to pass im=rgb2gray(im) as argument instead of a color image.";
    std::cout << std::endl;
  }
  else
    gray = *frame;

  int idx = -1;

  if(model.Track(gray, wSize2, fpd, nIter, clamp, fTol, fcheck) == 0)
    idx = model._clm.GetViewIdx();
  else
  {
    model.FrameReset();

    if(model.Track(gray, wSize1, fpd, nIter, clamp, fTol, fcheck) == 0)
      idx = model._clm.GetViewIdx();
  }

  //XXX: if idx == -1 then the tracker failed

  /* associate outputs */
  mwSize sts_dims = 1;
  STATUS = mxCreateNumericArray(1, &sts_dims, mxINT8_CLASS, mxREAL);
  int *status = (int *) mxGetPr(STATUS);
  *status = 0; // SUCCESS (default)

  if(idx == -1)
  {
    mwSize pts_dims = 0;
    POINTS  = mxCreateNumericArray(1, &pts_dims, mxDOUBLE_CLASS, mxREAL);
    *status = 1; // FAILURE
    return;
  }

  mwSize pts_dims[2] = { (mwSize) (model._shape.rows >> 1), (mwSize) 2};
  POINTS = mxCreateNumericArray(2, pts_dims, mxDOUBLE_CLASS, mxREAL);

  uint8 *pts = (uint8 *) mxGetPr(POINTS);

  SavePoints(pts, model._shape, model._clm._visi[idx]);

  /* free memory */
  delete frame;
}

void SavePoints(uint8 *pts, cv::Mat &shape, cv::Mat &visi)
{
  mwSize n = (mwSize) (shape.rows >> 1);

  for(mwSize i = 0; i < n; ++i)
    if(visi.at<int>(i, 0) != 0)
    {
      pts[i]   = shape.at<double>(i,   0);
      pts[i+n] = shape.at<double>(i+n, 0);
    }
}
