function [ scribble ] = get_scribble( image, target_name )
%GET_SCRIBBLE Snippet for getting scribbles for a given image:
% Usage:
%     [scribble] = get_scribble(image, target_name)
%
% Inputs:
% - image: image to be shown in the window
% - target_name: title (debug name) of the window
%
% Outputs:
% - scribble: binary mask

ttl = sprintf('Get scribble for %s', target_name);
msg = sprintf('Draw scribbles for %s - Press any key to draw scribbles - Press + to enlarge the scribbles and - to reduce it; press q when done', target_name);
waitfor(msgbox(msg, ttl));

mask = rgb2gray(draw_scribble(image));
%scribble = find(mask);
scribble = mask ~= 0;

end
