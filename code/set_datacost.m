function [ DataCost ] = set_datacost( DataCost, data_params, max_cost, penalty_params )
%GET_DATACOST Calculate and set DataCost.
% Usage:
%     [DataCost] = set_datacost(DataCost, data_params, max_cost, penalty_params)
%
% Inputs:
% - DataCost:    destination matrix for the DataCost
% - data_params: struct parameters for the DataCost calculation
%     - Cb: data matrix for Cb channel
%     - Cr: data matrix for Cr channel
%     - face: data struct for the face
%     - hair: data struct for the hair
%     - bkg:  data struct for the background
%         >>> - ind:  binary mask
%             - hist: histograms struct (see get_histograms function help)
% - max_cost:   for the DataCost
% - penalty_params: struct of parameters for the DataCost calculation (see apply_graphcut_segmentation function help)
%
% Outputs:
% - DataCost: H-by-W-by-(num labels) matrix for the result of the DataCost calculation ; (default) num labels = 3 -> face, hair, background

[H, W, ~] = size(DataCost);

% extract data parameters
face = data_params.face;
hair = data_params.hair;
bkg  = data_params.bkg;

Cb = data_params.Cb;
Cr = data_params.Cr;

% extract penalty parameters
phi_face = penalty_params.phi(1).data;
phi_hair = penalty_params.phi(2).data;

cen_face = penalty_params.centroids(1);
cen_hair = penalty_params.centroids(2);

frame = penalty_params.frame; % current frame number

beta  = penalty_params.beta;
gamma = penalty_params.gamma;
rho   = penalty_params.rho;

% calculate penalties
penalty_face = calculate_penalty(H, W, frame, beta, gamma, rho, phi_face, cen_face);
penalty_hair = calculate_penalty(H, W, frame, beta, gamma, rho, phi_hair, cen_hair);

% calculate costs
cost_face = face.hist.hist_Cb(Cb) .* face.hist.hist_Cr(Cr);
cost_hair = hair.hist.hist_Cb(Cb) .* hair.hist.hist_Cr(Cr);
cost_bkg  =  bkg.hist.hist_Cb(Cb) .*  bkg.hist.hist_Cr(Cr);

% set data costs
DataFace = -log(cost_face) + penalty_face;
if frame == 1
    DataFace(bkg.ind)  = max_cost;
    DataFace(hair.ind) = max_cost;
end
DataCost(:,:,1) = DataFace;

DataBkg = -log(cost_bkg);
if frame == 1
    DataBkg(face.ind) = max_cost;
    DataBkg(hair.ind) = max_cost;
end
DataCost(:,:,2) = DataBkg;

if ~isempty(find(hair.ind, 1))

    DataHair = -log(cost_hair) + penalty_hair;
    if frame == 1
        DataHair(bkg.ind)  = max_cost;
        DataHair(face.ind) = max_cost;
    end
    DataCost(:,:,3) = DataHair;
end

end
