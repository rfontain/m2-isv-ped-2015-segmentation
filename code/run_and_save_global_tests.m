%%RUN_AND_SAVE_GLOBAL_ERROR Is a simple script used for print tests results of global tests in a file.

clc;
close all;
clear all;

%% choose the sample id
num_sample=4;
%% define the steps between each frame (e.g. 20 takes 1 frame each 20 frames)
step=20;

%% other parameters used for the graphcut segmentations
%% XXX: do not forget to modify them if necessary !!!
N_pred   = 3;
beta     = 3;
gamma    = 5;

lambda   = 20;
num_bins = 16;
max_cost = 1000;

%% extract images from previous result of the application
infile=sprintf('../data/tests/sample%d_res.avi',num_sample);
outdir=sprintf('../data/tests/sample%d_res',num_sample);
avimov=aviread(infile); % array of structs : cdata=color_data_matrix, colormap=[]
n=length(avimov);
for i = 0:n-1
    if mod(i,20) == 0
        j=i+1;
        im=avimov(j).cdata;
        [h,w,d]=size(im);
        % convert single channel grayscale image into a RGB-grayscale image
        if d ~= 3
            im2=zeros(h,w,3,'uint8');
            im2(:,:,1)=im;
            im2(:,:,2)=im;
            im2(:,:,3)=im;
            im=im2;
        end
        outfile=sprintf('image-%05d.png',j);
        outfile=fullfile(outdir,outfile);
        imwrite(im,outfile,'png');
    end
end

%% folders filenames for the global error calculation
indirref=sprintf('../data/tests/sample%d_ref',num_sample);
indirres=sprintf('../data/tests/sample%d_res',num_sample);
%% output text file filename
outfile=sprintf('../data/tests/sample%d_tests.txt',num_sample);

%% global success calculation
[gsucc,n,h,w,d]=test_reussite(indirref, indirres);

%% dump results in a file
fp=fopen(outfile,'w');
fprintf(fp,'Results of global error between:\n');
fprintf(fp,'- "%s"\n',indirref);
fprintf(fp,'- "%s"\n',indirres);
fprintf(fp,'\n\n');
fprintf(fp,'Other segmentation parameters:\n');
fprintf(fp,'- N frames prediction = %d\n', N_pred);
fprintf(fp,'- beta = %d\n', beta);
fprintf(fp,'- gamma = %d\n', gamma);
fprintf(fp,'- lambda = %d\n', lambda);
fprintf(fp,'- num of bins (histogram) = %d\n', num_bins);
fprintf(fp,'- max cost (DataCost) = %d\n', max_cost);
fprintf(fp,'\n\n');
fprintf(fp,'Results:\n');
fprintf(fp,'- image dimensions (HxWxD) = (%d,%d,%d)\n',h,w,d);
fprintf(fp,'- number of images in a single folder = %d\n',n);
fprintf(fp,'- global success = %.2f%%\n',gsucc);
fprintf(fp,'- global error = %.2f%%\n',(100-gsucc));
fclose(fp);
