function cleanup(  )
%CLEANUP CLeaning variable, opened figures and environment
%   CLeaning variable, opened figures and environment
    close all;
    clear all;
    clc;
end
