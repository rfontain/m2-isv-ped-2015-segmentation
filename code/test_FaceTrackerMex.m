function [points, status] = test_FaceTrackerMex(varargin)
%% TEST_FACETRACKERMEX Run a test for FaceTracker using mex and opencv libs.
%%
%% Usage:
%%   [points, status] = test_FaceTrackerMex()
%%   [points, status] = test_FaceTrackerMex(compile)
%%
%% Inputs:
%% - debug: 0 (default) then (re)compile the MEX file ; 1 then do not (re)compile
%%
%% Outputs:
%% - points: 2D-double array of tracking points
%% - status: output status, 0 then success ; 1 then failure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;

if length(varargin) < 1 || varargin{1} == 1
  % You can omit `-v` to disable the verbose mode with mex
  mex -v -largeArrayDims ...
    CXX="g++" CXXFLAGS="\$CXXFLAGS -std=c++11 -Wextra -Wall -pedantic-errors -O3 -Wno-long-long" ...
    -I/usr/local/include -IFaceTracker/include ...
    -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_objdetect ...
    FaceTrackerMex.cc ...
    FaceTracker/src/lib/IO.cc FaceTracker/src/lib/PDM.cc FaceTracker/src/lib/Patch.cc FaceTracker/src/lib/CLM.cc FaceTracker/src/lib/FDet.cc FaceTracker/src/lib/PAW.cc FaceTracker/src/lib/FCheck.cc FaceTracker/src/lib/Tracker.cc
end

dataDir='../data';
sampDir='_sample1.mpeg';

% im=zeros(4,4);
% im=zeros(4,4,3,'uint8');
% im=uint8(rand(4,4)*10)
im=imread(fullfile(dataDir, sampDir, 'image-00001.png'));
im=rgb2gray(im); % TODO: make cv::cvtColor(src, dst, CV_RGB2GRAY) work
whos('im')

modelDir='FaceTracker/model';

ftFile=fullfile(modelDir,'face.tracker');
conFile=fullfile(modelDir,'face.con');
triFile=fullfile(modelDir,'face.tri');
fcheck=1;

% FaceTrackerMex()
% points=FaceTrackerMex(im, ftFile, conFile, triFile, fcheck);
[points, status]=FaceTrackerMex(im, ftFile, conFile, triFile, fcheck);

end
