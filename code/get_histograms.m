function [ histo ] = get_histograms( im_YCbCr, mask, num_bins )
%GET_HISTOGRAM Calculate the histograms of the given image.
% Usage:
%     [histo] = get_histograms(im_YCbCr, mask, num_bins)
%
% Inputs:
% - im_YCbCr: double, matrix of the image in YCbCr color space
% - mask:     gives the values to be used for the calculation
% - num_bins: of the histograms to be used for the calculation
%
% Outputs:
% - histo: struct
%     - hist_Cb: histogram for the Cb channel
%     - hist_Cr: histogram for the Cr channel

im_Cb = im_YCbCr(:,:,2);
im_Cr = im_YCbCr(:,:,3);

hist_Cb = imhist(im_Cb(mask), num_bins);
hist_Cr = imhist(im_Cr(mask), num_bins);

hist_Cb = hist_Cb/sum(hist_Cb) + 0.000001;
hist_Cr = hist_Cr/sum(hist_Cr) + 0.000001;

histo = struct('hist_Cb', hist_Cb, 'hist_Cr', hist_Cr);

end
