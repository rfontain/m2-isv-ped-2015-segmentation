function [ penalty ] = calculate_penalty( H, W, frame, beta, gamma, rho, phi, centroids )
%CALCULATE_PENALTY Calculate penalty matrix for the DataCost calculation.
% Usage:
%   [penalty] = calculate_penalty(H, W, frame, beta, gamma, rho, phi, centroids)
%
% Inputs:
% - H, W:  height (num of rows) and width (num of cols) of the image
% - frame: current frame id
% - beta:  beta > 0, the weight for relative distance penalty influence
% - gamma: theorical max move (in px) between two frames
% - rho:   buffer, actually equals to gamma/2
% - phi:   struct for the distance penalties
%     - name: debug name
%     - data: actual values
% - centroids: struct
%     - name: debug name
%     - dist: array of previous distances calculated for the N=length(dist) last frames
%     - previous: previous coordinates (row, col)
%
% Outputs:
% - penalty: matrix of the calculated distance penalty ; (default) or a zeros filled H-by-W matrix

if isempty(phi) % then mask (phi) is not defined
    penalty = zeros(H, W);
else
    alpha   = calculate_alpha(frame, gamma, rho, centroids);
    penalty = beta * alpha * phi;
end

end
