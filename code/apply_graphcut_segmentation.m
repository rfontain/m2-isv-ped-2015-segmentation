function [ im_result, label_result ] = apply_graphcut_segmentation( im_YCbCr, masks, gc_params, penalty_params )
%APPLY_GRAPHCUT_SEGMENTATION Takes an image in YCbCr color space and apply the GraphCut based segmentation giving the masks and specific parameters.
% Usage:
%   [im_result, label_result] = apply_graphcut_segmentation(im_YCbCr, masks, gc_params, penalty_params)
%
% Inputs:
% - im_YCbCr: double matrix of an image in YCbCr color space
% - masks: struct
%       - face: matrix of indices corresponding with the face
%       - hair: matrix of indices corresponding with the hair
%       - bkg:  matrix of indices corresponding with the background
% - gc_params: struct of parameters for the GraphCut segmentation
%       - lambda:    lambda >= 0, weight for relative influence
%       - num_bins:  for the histogram calculation
%       - max_cost:  for the DataCost
%       - default:   array of user default masks for the first frame ; used if not empty
%       - scribbles: array of user default scribbles for the first frame ; used if not empty
%       - box:       array of user default bounding-box for adjusting the first frame segmentation ; used if not empty
%       - face_landmarks: array of user landmarks for face for the first frame ; used if not empty
% - penalty_params: struct of parameters for the DataCost calculation
%       - frame: current frame id
%       - beta:  beta > 0, the weight for relative distance penalty influence
%       - gamma: theorical max move (in px) between two frames
%       - rho:   buffer, actually equals to gamma/2
%       - phi:   array of structs for the distance penalties
%           - name: debug name
%           - data: actual values
%       - centroids: array of structs for the centroids
%           - name: debug name
%           - dist: array of previous distances calculated for the N=length(dist) last frames
%           - previous: previous coordinates (row, col)
%
% Outputs:
% - im_result: grayscale image of the segmented image
%     Gray values : 255=face, 127=hair, 0=background
% - label_result: label of the GraphCut segmentation, for the mask propagation

[H, W, ~] = size(im_YCbCr);

im_Cb = im_YCbCr(:,:,2);
im_Cr = im_YCbCr(:,:,3);

ind_face = masks.face;
ind_hair = masks.hair;
ind_bkg  = masks.bkg;

n_graphs = length(fieldnames(masks));

if isempty(find(ind_hair, 1))
    n_graphs = n_graphs - 1;
end

lambda   = gc_params.lambda;
max_cost = gc_params.max_cost;
num_bins = gc_params.num_bins;

%% 1. Calculate histograms
hist_face = get_histograms(im_YCbCr, ind_face, num_bins);
hist_hair = get_histograms(im_YCbCr, ind_hair, num_bins);
hist_bkg  = get_histograms(im_YCbCr, ind_bkg,  num_bins);

%% 1.bis Create DataCost parameters
data_face = struct('ind', ind_face, 'hist', hist_face);
data_hair = struct('ind', ind_hair, 'hist', hist_hair);
data_bkg  = struct('ind', ind_bkg,  'hist', hist_bkg);

data_Cb = uint8(im_Cb(:,:) * (num_bins - 1) + 1); % values from 1 to num_of_bins
data_Cr = uint8(im_Cr(:,:) * (num_bins - 1) + 1); % values from 1 to num_of_bins

data_params = struct('Cb', data_Cb, 'Cr', data_Cr, 'face', data_face, 'hair', data_hair, 'bkg', data_bkg);

%% 2. Calculate DataCost
DataCost = zeros(H, W, n_graphs);
DataCost = set_datacost(DataCost, data_params, max_cost, penalty_params);

%% 3. Calculate Vertical and Horizontal costs
[v_cost, h_cost] = get_smoothness_costs(im_YCbCr, lambda);

%% 3.bis Calculate SmoothnessCost
SmoothnessCost = ones(n_graphs) - eye(n_graphs);

%% 4. Apply GraphCut
gch      = GraphCut('open', DataCost, SmoothnessCost, v_cost, h_cost);
[gch, L] = GraphCut('swap',gch);
GraphCut('close', gch);

%% 5. Calculate Segmented Image
seg_face = L == 0;
seg_bkg  = L == 1;
seg_hair = L == 2;

im_result = zeros(H, W, 'uint8');
im_result(seg_bkg)  = 0;   % background
im_result(seg_hair) = 127; % hair
im_result(seg_face) = 255; % face

label_result = L;

end
