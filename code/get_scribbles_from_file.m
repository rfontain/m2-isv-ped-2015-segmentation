function [scribbles] = get_scribbles_from_file(filename, im)
%GET_SCRIBBLES_FROM_FILE Read and load scribbles mask from a file.
% Usage:
%     [scribbles] = get_scribbles_from_file(filename, im)
%
% Inputs:
% - filename: of the input file
% - im: image matrix, given for the size
%
% Outputs:
% - scribbles: binary mask

file = fopen(filename, 'r');

formatSpec = '%d %d';
sizeF = [2 Inf];
f = fscanf(file, formatSpec, sizeF);
f = f';

i = f(:,2);
j = f(:,1);

[H W ~] = size(im);

idx = sub2ind(size(im),i,j);

mask = zeros(H,W);
mask(idx) = 1;

scribbles = mask ~= 0;

fclose(file);

end
