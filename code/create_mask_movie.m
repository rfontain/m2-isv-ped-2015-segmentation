function [ mov, scribbles, first_seg, b_box, summary ] = create_mask_movie( input_dir_filename, output_filename, gc_params, beta, gamma, N )
%CREATE_MASK_MOVIE Create and write a movie from all the GraphCut segmented image contained in input_dir.
% Usage:
%   [mov, scribbles, first_seg, b_box, summary] = create_mask_movie(input_dir_filename, output_filename, gc_params, beta, gamma, N)
%
% Inputs:
% - input_dir_filename: the filename of a folder that contains all the image to be used and segmented
% - output_filename:    the filename of the output video
% - gc_params: struct of parameters for the GraphCut segmentation
%       - lambda
%       - num_bins
%       - max_cost
%       - default:   array of user default masks for the first frame ; used if not empty
%       - scribbles: array of user default scribbles for the first frame ; used if not empty
%       - box:       array of user default bounding-box for adjusting the first frame segmentation ; used if not empty
%       - face_landmarks: array of user landmarks for face for the first frame ; used if not empty
% - beta:  beta > 0, the weight for relative distance penalty influence
% - gamma: theorical max move (in px) between two frames
% - N: number of frames to use for the prediction
%
% Output:
% - mov: array of structs that represent the movie data
%     - cdata:    color data for each frame
%     - colormap: (default) empty matrix
% - scribbles: H-by-W-by-3 matrix, color image of the scribbles used for the first frame segmentation
%     - 1st channel: face scribbles
%     - 2nd channel: hair scribbles
%     - 3rd channel: background scribbles
% - b_box:   bounding-box used for adjusting the first frame segmentation
% - summary: summary window/figure

%% 1. Get all files contained in a directory
file_list = get_all_files(input_dir_filename);

num_images = size(file_list, 1);

%% 2. Read the first frame
im = imread(char(file_list(1)));

[H, W, C] = size(im);

%% 2.bis Get scribbles of the first frame
if ~isempty(gc_params.default)
    def = gc_params.default;
    ind_face = def == 255;
    ind_hair = def == 127;
    ind_back = def == 0;

    scribbles = [];
elseif ~isempty(gc_params.scribbles)
    scr = gc_params.scribbles;
    ind_face = logical(scr(:,:,1));
    ind_hair = logical(scr(:,:,2));
    ind_back = logical(scr(:,:,3));

    scribbles = [];
else
    if ~isempty(gc_params.face_landmarks)
        face_landmarks_file = gc_params.face_landmarks;
        ind_face = get_scribbles_from_file(face_landmarks_file, im);
    else
        ind_face = get_scribble(im, 'face');
    end
    ind_hair = get_scribble(im, 'hair');
    ind_back = get_scribble(im, 'background');
    clc;

    scribbles = zeros(H, W, C, 'uint8');
    scribbles(:,:,1) = 255 * ind_face;
    scribbles(:,:,2) = 255 * ind_hair;
    scribbles(:,:,3) = 255 * ind_back;
end

masks = struct(...
    'face', ind_face, ...
    'hair', ind_hair, ...
    'bkg',  ind_back);

%% 2.ter Get bounding box
%   in order to adjust the masks of the first frames
if ~isempty(gc_params.box)
    b_box = gc_params.box;
elseif isempty(gc_params.default)
    b_box = get_bounding_box(im);
else
    b_box = [];
end

%% 3. Prepare GraphCut parameters and output movie
penalty_params = struct(...
    'frame', 0,     ...
    'beta',  beta,  ...
    'gamma', gamma, ...
    'rho',   gamma/2,   ...
    'phi',       repmat(struct('name', '', 'data', []), 1, 2), ...
    'centroids', repmat(struct('name', '', 'previous', zeros(1, 2), 'dist', zeros(1, N)), 1, 2));

% assign debug names
penalty_params.phi(1).name = 'face';
penalty_params.phi(2).name = 'hair';

penalty_params.centroids(1).name = 'face';
penalty_params.centroids(2).name = 'hair';

% allocate mov data strut
mov = repmat(struct('cdata', zeros(H, W, C, 'uint8'), 'colormap', []), 1, num_images);

%% 4. Apply GraphCut on all images
tic_start = tic;

for i = 1 : num_images

   im = imread(char(file_list(i)));
   im = im2double(rgb2ycbcr(im));

   penalty_params.frame = i;

   [result, labels] = apply_graphcut_segmentation(im, masks, gc_params, penalty_params);

   if i == 1
        if ~isempty(b_box)
            % Adjust masks using selected bounding box
            result(~b_box) = 0;
            labels(~b_box) = 1; % assign label of background

            first_seg = result;
        else
            first_seg = [];
        end

       % display a summary window of the first segmentation
       close all;

       summary = figure('name', 'Segmentation - Summary Window');
       subplot(2, 3, [1 3]); imshow(result);
       title('Result on the first frame');

       subplot(2, 3, 4); imshow(labels == 0);
       title('Face mask');

       subplot(2, 3, 5); imshow(labels == 1);
       title('Background mask');

       subplot(2, 3, 6); imshow(labels == 2);
       title('Hair mask');

       choice = questdlg('Do you want to continue?', 'Segmentation - Summary Window', 'Yes', 'No', 'Yes');

       clc;

       switch choice
           case 'Yes'
               disp('Continue the segmentation and the creation of the movie.');
               disp('You can close the summary window at any time.');
               disp('Please wait...');
           case 'No'
               close all;
               break
       end
   end

   % propagate masks
   masks.face = labels == 0;
   masks.bkg  = labels == 1;
   masks.hair = labels == 2;

   % update mov data struct
   mov(i).cdata    = result;
   mov(i).colormap = [];

   %% Update penalty params

   % distance penalty
   penalty_params.phi(1).data = bwdist(masks.face);
   penalty_params.phi(2).data = bwdist(masks.hair);

   % calculate centroids
   [r_face, c_face] = find(result == 255);
   [r_hair, c_hair] = find(result == 127);

   r_face = floor(mean(r_face));
   c_face = floor(mean(c_face));

   %% debug centroids - 1
   % rr = r_face;
   % cc = c_face;
   % mov(i).cdata(:,:,1) = result;
   % mov(i).cdata(:,:,2) = result;
   % mov(i).cdata(:,:,3) = result;
   % mov(i).cdata(rr-2:rr+2, cc-2:cc+2, 1) = 255;
   % mov(i).cdata(rr-2:rr+2, cc-2:cc+2, 2) = 0;
   % mov(i).cdata(rr-2:rr+2, cc-2:cc+2, 3) = 255;

   r_hair = floor(mean(r_hair));
   c_hair = floor(mean(c_hair));

   %% debug centroids - 2
   % rr = r_hair;
   % cc = c_hair;
   % mov(i).cdata(rr-2:rr+2, cc-2:cc+2, 1) = 255;
   % mov(i).cdata(rr-2:rr+2, cc-2:cc+2, 2) = 255;
   % mov(i).cdata(rr-2:rr+2, cc-2:cc+2, 3) = 0;

   if i == 1
       penalty_params.centroids(1).previous = [r_face c_face];
       penalty_params.centroids(2).previous = [r_hair c_hair];
   end

   % calculus for the face
   dr = r_face - penalty_params.centroids(1).previous(1);
   dc = c_face - penalty_params.centroids(1).previous(2);

   penalty_params.centroids(1).previous = [r_face c_face];

   penalty_params.centroids(1).dist(mod(i, N) + 1) = norm([dr dc]);

   % calculus for the hair
   dr = r_hair - penalty_params.centroids(2).previous(1);
   dc = c_hair - penalty_params.centroids(2).previous(2);

   penalty_params.centroids(2).previous = [r_hair c_hair];

   penalty_params.centroids(2).dist(mod(i, N) + 1) = norm([dr dc]);

end

toc(tic_start);

%% 5. Create movie from masks
if penalty_params.frame == num_images
    movie2avi(mov, output_filename, 'compression', 'None');
    out_str = sprintf('Movie saved at "%s"', output_filename);
    disp(out_str);
else
    disp('Execution aborted.');
    mov = [];
end

end
