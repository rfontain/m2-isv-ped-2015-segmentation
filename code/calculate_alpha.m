function [ alpha ] = calculate_alpha( frame, gamma, rho, centroids )
%CALCULATE_ALPHA Calculate alpha coefficient for the DataCost calculation.
% Usage:
%   [alpha] = calculate_alpha(frame, gamma, rho, centroids)
%
% Inputs:
% - frame: current frame id
% - gamma: theorical max move (in px) between two frames
% - rho:   buffer, actually equals to gamma/2
% - centroids: struct
%     - name: debug name
%     - dist: array of previous distances calculated for the N=length(dist) last frames
%     - previous: previous coordinates (row, col)
%
% Outputs:
% - alpha: alpha coefficient

if frame > 1

    N_pred = length(centroids.dist); % prediction size

    if frame < N_pred
        dist_prev = centroids.dist(1:frame-1);
    else
        cen = mod(frame, N_pred) + 1; % current centroid id
        dist_prev = centroids.dist([1:cen-1 cen+1:end]);
    end

    x = mean(dist_prev); % average displacement in the past few frames

    alpha = exp(- min(x, gamma).^2 / rho.^2);

else

    alpha = 1;

end

end
