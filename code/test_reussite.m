function [ global_percent, num_images, H, W, D ] = test_reussite( dir_frame_ref, dir_frame_res )
%TEST_DIFFERENCE Calculate the success rate between reference segmented frames and frames that has been segmented by the application.
% Usage:
%     [ouput_args] = test_reussite(dir_frame_ref, dir_frame_res)
%
% Inputs:
% - dir_frame_ref: filename of the input folder that contains all the reference frames
% - dir_frame_res: filename of the input folder that contains all the result frames
%
% Outputs:
% - global_percent: of the success rate
% - num_images: (of a single folder - references) used for the calculation
% - H, W, D: dimensions of the images

% Get all files contained in a directory
file_list_ref = get_all_files(dir_frame_ref);
file_list_res = get_all_files(dir_frame_res);

num_images = size(file_list_ref, 1);

% Read the first frame for size purpose
frame1_ref = imread(char(file_list_ref(1)));

[H, W, D] = size(frame1_ref);
pix_tot = H*W;

global_percent = 0;
percent_tab = zeros(10,1);

for i = 1:num_images

    frame_ref = imread(char(file_list_ref(i)));
    frame_res = imread(char(file_list_res(i)));

    frame_ref = im2double(rgb2gray(frame_ref));
    frame_res = im2double(rgb2gray(frame_res));

    % difference between frames : white pixels are correct, black are not
    diff = frame_ref == frame_res;

    pix_ok = length(find(diff == 1));

    percent = (pix_ok/pix_tot)*100;
    global_percent = global_percent + percent;
    percent_tab(i) = percent;

end

global_percent = global_percent / num_images;
percent_tab

end
