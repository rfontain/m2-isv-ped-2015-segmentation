%function [gch se de] = script_graphcut_segmentation()
%%SCRIPT_GRAPHCUT_SEGMENTATION Run a simple test for the GraphCut segmentation.
clc;
clear all;
close all;
addpath('GC/');

%% 1. Get Image
im = imread('../data/ID02_001.bmp');

%% 1.bis Get Scribbles for Face, Hair and Background
ind_face  = get_scribble(im, 'face');
ind_hair  = get_scribble(im, 'hair');
ind_bkg   = get_scribble(im, 'background');

%% 2. Apply GraphCut
[H, W, ~] = size(im);
im = im2double(rgb2ycbcr(im));

% Conversion from RGB to YCbCR
% Y: luminance
% Cb, Cr: chrominance
im_YCbCr  = struct('W', W, 'H', H, 'Y', im(:,:,1), 'Cb', im(:,:,2), 'Cr', im(:,:,3));
scribbles = struct('face', ind_face, 'hair', ind_hair, 'bkg', ind_bkg);
gc_params = struct('lambda', 20, 'num_bins', 16, 'max_cost', 1000);
penalty_params = struct('phi', 0, 'beta', 2, 'gamma', 5, 'rho', 0, 'centroids', []);

% temp
penalty_params.rho = penalty_params.gamma / 2;

result = apply_graphcut_segmentation(im_YCbCr, scribbles, gc_params, penalty_params);

%% 3. Show Result
close all; % close all junk frames from draw_scribble and other
figure; imshow(result);
