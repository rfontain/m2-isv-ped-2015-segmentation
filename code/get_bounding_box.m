function [b_box] = get_bounding_box(im)
%GET_BOUNDING_BOX Create a bounding-box from the coordinates defined by the user.
% Usage:
%   [b_box] = get_bounding_box(im)
%
% Inputs:
% - im: image to be shown in the window
%
% Outputs:
% - b_box: im sized matrix of indices
%     - b_box(:) == 0 then the corresponding value of the image is not in the bounding-box
%     - b_box(:) ~= 0 otherwise

[H W ~] = size(im);
imshow(im)
title('click on top left corner and bottom right corner to select a bounding box for face and hair')
input_pts = ginput(2);
while sum(input_pts(:) < 1) || sum([ input_pts(1,1), input_pts(2,1)] > W) ...
        || sum([ input_pts(1,2), input_pts(2,2)] > H)
    title('please click in the image')
    input_pts = ginput(2);
end

i1 = input_pts(1,2);
j1 = input_pts(1,1);

i2 = input_pts(2,2);
j2 = input_pts(2,1);

b_box = zeros(H,W);
b_box(i1:i2,j1:j2) = 1;
end
