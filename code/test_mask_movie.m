%%TEST_MASK_MOVIE Run a simple test for the creation of a movie of the segmented images of a video.
%% Images are extracted in a folder, then we get all these images and we apply a segmentation.
%% This is the main script of the final application.

cleanup;

addpath('GC/')

input_directory = '../data/_sample8.mpeg';
output_filename = '../data/output-sample8.avi';

N_pred = 3;
beta   = 3;
gamma  = 5;

gc_params = struct(...
    'lambda',         20,   ...
    'num_bins',       16,   ...
    'max_cost',       1000, ...
    'default',        [],   ... % user default mask for the first frame
    'scribbles',      [],   ... % user default scribbles for the first frame
    'face_landmarks', [],   ... % user landmarks for face for the first frame
    'box',            []    ... % user default bounding-box for adjusting the first frame segmentation
    );

choice = questdlg('Do you want to load a default segmentation for the first frame?', 'Load default segmentation', 'Yes', 'No', 'Yes');

switch choice
    case 'Yes'
        [file, path] = uigetfile('*', 'Choose a file');

        if ischar(file)
            filename = fullfile(path, file);
            def_seg  = imread(filename);
            if size(def_seg, 3) == 3
                def_seg = rgb2gray(def_seg);
            end
            gc_params.default = def_seg;
        end
end

if isempty(gc_params.default)
    choice = questdlg('Do you want to load default scribbles for the first frame?', 'Load default scribbles', 'Yes', 'No', 'Yes');

    switch choice
        case 'Yes'
            [file, path] = uigetfile('*', 'Choose a file');

            if ischar(file)
                filename = fullfile(path, file);
                def_scribbles = imread(filename);
                gc_params.scribbles = def_scribbles;
            end
    end
end

if ~isempty(gc_params.scribbles)
    choice = questdlg('Do you want to load a default bounding-box for the first frame segmentation?', 'Load default bounding-box', 'Yes', 'No', 'Yes');

    switch choice
        case 'Yes'
            [file, path] = uigetfile('*', 'Choose a file');

            if ischar(file)
                filename = fullfile(path, file);
                def_box = imread(filename);
                if size(def_box, 3) == 3
                    def_box = rgb2gray(def_box);
                end
                def_box = logical(def_box);
                gc_params.box = def_box;
            end
    end
end

if isempty(gc_params.default) && isempty(gc_params.scribbles)
    choice = questdlg('Do you want to load a file containing the scribbles coordinates (from FaceTracker) for the first frame?', 'Load file containing scribbles coordinates', 'Yes', 'No', 'Yes');

    switch choice
        case 'Yes'
            [file, path] = uigetfile('*', 'Choose a file');

            if ischar(file)
                filename = fullfile(path, file);
                gc_params.face_landmarks = filename;
            end
    end
end

[mov, scribbles, first_seg, box, ~] = create_mask_movie(input_directory, output_filename, gc_params, beta, gamma, N_pred);

if ~isempty(mov)
    msg = sprintf('Movie saved at "%s"', output_filename);
    waitfor(msgbox(msg, 'Movie saved!'));
else
    waitfor(msgbox('Execution aborted.', 'Execution aborted!'));
end

if ~isempty(first_seg)
   choice = questdlg('Do you want to save the first frame segmentation?', 'First frame segmentation', 'Yes', 'No', 'Yes');

   switch choice
       case 'Yes'
           [file, path, ~] = uiputfile('*.bmp', 'Choose a location for segmentation file');

           if ischar(file)
               filename = fullfile(path, file);

               if size(first_seg, 3) == 3
                   first_seg = rgb2gray(first_seg);
               end

               imwrite(first_seg, filename, 'bmp');

               msg = sprintf('Segmentation file saved at "%s".', filename);
               disp(msg);
               waitfor(msgbox(msg, 'First frame segmentation'));
           end
   end
end

if ~isempty(scribbles)
  choice = questdlg('Do you want to save the scribbles used for the first frame segmentation?', 'Scribbles', 'Yes', 'No', 'Yes');

  switch choice
    case 'Yes'
      [file, path, ~] = uiputfile('*.bmp', 'Choose a location for scribbles file');

      if ischar(file)
        filename = fullfile(path, file);

        imwrite(scribbles, filename, 'bmp');

        msg = sprintf('Scribbles file saved at "%s".', filename);
        disp(msg);
        waitfor(msgbox(msg, 'Scribbles'));
      end
  end
end

if ~isempty(box)
   choice = questdlg('Do you want to save the bounding-box?', 'Bounding-box', 'Yes', 'No', 'Yes');

   switch choice
       case 'Yes'
           [file, path, ~] = uiputfile('*.bmp', 'Choose a location for bounding-box file');

           if ischar(file)
               filename = fullfile(path, file);

               box = uint8(255 * logical(box));

               imwrite(box, filename, 'bmp');

               msg = sprintf('Bounding-box file saved at "%s".', filename);
               disp(msg);
               waitfor(msgbox(msg, 'Bounding-box'));
           end
   end
end

close all;
